const Sequelize = require('sequelize');

const sequelize = require('../_utils/database');

const Post = sequelize.define('posts', {
  id: {
    type: Sequelize.INTEGER,
    autoIncrement: true,
    allowNull: false,
    primaryKey: true,
  },
  title: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  body: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  author: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  urlSlug: {
    type: Sequelize.STRING,
    allowNull: false,
  },

});

module.exports = Post;
