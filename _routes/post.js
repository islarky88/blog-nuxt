const express = require('express');

const postController = require('../_controllers/post');

const router = express.Router();

router.post('/new', postController.newPost);
router.get('/all', postController.getAllPost);
router.get('/:slug([A-Za-z0-9-]+)', postController.getSinglePost);
router.delete('/:id([0-9]+)', postController.deletePost);
router.patch('/:id([0-9]+)', postController.editPost);

module.exports = router;
