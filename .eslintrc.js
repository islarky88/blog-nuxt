module.exports = {
	root: true,
	env: {
		browser: true,
		node: true
	},
	parserOptions: {
		parser: 'babel-eslint'
	},
	extends: [ '@nuxtjs', 'plugin:nuxt/recommended', 'eslint:recommended', 'plugin:vue/essential' ],
	plugins: [ 'vue' ],
	// add your custom rules here
	rules: {
		'nuxt/no-cjs-in-config': 'off',
		indent: [ 'error', 2 ],
		semi: [ 'error', 'always' ],
		'space-before-function-paren': [
			'error',
			{
				anonymous: 'never',
				named: 'never',
				asyncArrow: 'never'
			}
		],
		'comma-dangle': [ 'error', 'always-multiline' ],
		// 'vue/attribute-hyphenation': [ 'error', 'never' ],
		'vue/html-closing-bracket-newline': [
			'error',
			{
				singleline: 'never',
				multiline: 'always'
			}
		],
		'vue/html-self-closing': [
			'error',
			{
				html: {
					void: 'always',
					normal: 'never',
					component: 'never'
				},
				svg: 'always',
				math: 'always'
			}
		],
		'no-multiple-empty-lines': [ 'error', { max: 1, maxEOF: 0 } ],
		'no-console': [ 'error', { allow: [ 'warn', 'log' ] } ]
	}
};
