const path = require('path');
const dotenv = require('dotenv');

if (process.env.NODE_ENV === 'development') {
  dotenv.config({ path: path.join(__dirname, '/.env.dev') });
  console.log(path.join(__dirname, '/.env.dev'));
  console.log('DEVELOPLEMT MODE using .env.dev');
} else {
  dotenv.config();
  console.log('PRODUCTION MODE using .env');
}

console.log('db connection' + process.env.DB_CONNECTION);

const port = process.env.APP_PORT;

const express = require('express');
const consola = require('consola');
const { Nuxt, Builder } = require('nuxt');

const bodyParser = require('body-parser');

// Routes
const postRoutes = require('./_routes/post');

// Models
// const Post = require('./_models/episode');

const app = express();

// Disable Certain Headers
app.disable('x-powered-by');

// app.use(bodyParser.urlencoded()); // x-www-form-urlencoded <form>
app.use(bodyParser.json()); // application/json
// app.use(multer({ storage: fileStorage, fileFilter: fileFilter }).single('image'));
// app.use('/_nuxt', express.static(path.join(__dirname, '.nuxt/dist/client')));
//

// CORS header
app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'OPTIONS, GET, POST, PUT, PATCH, DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization');
  next();
});

// API Routes
// app.use('/api/admin', adminRoutes);
// app.use('/api/auth', authRoutes);
app.use('/api/post', postRoutes);
// app.use('/api', seriesRoutes);

// API error catcher
app.use((error, req, res, next) => {
  console.log('ERRORS FOUND: ' + error.message);
  const status = error.statusCode || 500;
  const message = error.message;
  const data = error.data;
  res.status(status).json({ error: message, data });
});

// Database Associations
// Series.hasMany(Episode);
// Episode.belongsTo(Series);
// Episode.hasMany(Source);
// Source.belongsTo(Episode);

// Import and Set Nuxt.js options
const config = require('./nuxt.config.js');
config.dev = process.env.NODE_ENV !== 'production';

async function start() {
  // Init Nuxt.js
  const nuxt = new Nuxt(config);

  const { host } = nuxt.options.server;

  // Build only in dev mode
  if (config.dev) {
    const builder = new Builder(nuxt);
    await builder.build();
  } else {
    await nuxt.ready();
  }

  // Give nuxt middleware to express
  app.use(nuxt.render);

  // Listen the server
  app.listen(port, host);
  consola.ready({
    message: `Server listening on http://${host}:${port}`,
    badge: true,
  });
}

if (process.env.NODE_API_ONLY) {
  app.listen(port, () => console.log(`Node Express without NUXT ${port}!`));
} else {
  start();
}
