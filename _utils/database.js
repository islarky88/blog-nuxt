const Sequelize = require('sequelize');

// const sequelize = new Sequelize(
//   'mysql://root:123456@localhost:3306/' + process.env.DB_DATABASE
// )

console.log(process.env.DB_DATABASE);
console.log(process.env.DB_USERNAME);
console.log(process.env.DB_PASSWORD);
console.log(process.env.DB_HOST);
console.log(process.env.DB_CONNECTION);

const sequelize = new Sequelize(
  process.env.DB_DATABASE,
  process.env.DB_USERNAME,
  process.env.DB_PASSWORD,
  {
    host: process.env.DB_HOST,
    dialect: process.env.DB_CONNECTION,
  }
);

module.exports = sequelize;
