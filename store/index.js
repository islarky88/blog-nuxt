export const state = () => ({
  appName: 'Ace Nuxt Blog',
  menuItems: [
    // { name: 'Dashboard', icon: '', link: '/dashboard' },
    { name: 'About', icon: '', link: '/about' },
    { name: 'Travel', icon: '', link: '/travel' },
    { name: 'Leisure', icon: '', link: '/leisure' },
    { name: 'My Articles', icon: '', link: '/dashboard/my-articles' },
    // { name: 'Leisure', icon: '', link: '/leisure' },
    // { name: 'Travel', icon: '', link: '/travel' },
    // { name: 'Leisure', icon: '', link: '/leisure' },
    // { name: 'Technology', icon: '', link: '/technology' },

  ],

  authenticatedUser: '',
});

export const mutations = {
  add(state, text) {
    state.list.push({
      text,
      done: false,
    });
  },
  remove(state, { todo }) {
    state.list.splice(state.list.indexOf(todo), 1);
  },
  toggle(state, todo) {
    todo.done = !todo.done;
  },
};
