// const cryptoRandomString = require('crypto-random-string');
const slugify = require('slugify');
// const { validationResult } = require('express-validator');
// const errorFormatter = ({ location, msg, param, value, nestedErrors }) => {
//   // Build your resulting errors however you want! String, object, whatever - it works!
//   // return `${location}[${param}]: ${msg}`
//   return `${param}: ${msg}`;
// };

// const Sequelize = require('sequelize');

const Post = require('../_models/post');

exports.getSinglePost = async(req, res, next) => {
  const slug = req.params.slug;

  console.log('single episode search: ' + slug);

  try {
    const postData = await Post.findOne({
      // attributes: ['id', 'title', 'url'],
      raw: true,
      where: { urlSlug: slug },
    });
    console.log('single episode search: ' + slug);

    if (!postData) {
      const error = new Error('Could not find episode.');
      error.statusCode = 404;
      throw error;
    }

    const response = {
      postData,
    };

    // { firstName: 'Foo', age: 22, lastName: 'Bar', gender: 'M', planet: 'Earth' }

    res.status(200).json(response);
  } catch (error) {
    if (!error.statusCode) {
      error.statusCode = 500;
    }
    next(error);
  }
};

exports.getAllPost = async(req, res, next) => {
  try {
    const postData = await Post.findAll({
      // attributes: ['id', 'title', 'url'],

    });

    if (!postData) {
      const error = new Error('Could not find episode.');
      error.statusCode = 404;
      throw error;
    }

    const response = {
      postData,
    };

    res.status(200).json(response);
  } catch (error) {
    if (!error.statusCode) {
      error.statusCode = 500;
    }
    next(error);
  }
};

exports.newPost = async(req, res, next) => {
  // const result = validationResult(req).formatWith(errorFormatter);

  const title = req.body.title;
  const body = req.body.body;
  const author = req.body.author;
  const slug = req.body.url || slugify(title, { lower: true });

  try {
    const postSlug = await Post.findOne({
      // attributes: ['id', 'title', 'url'],
      raw: true,
      where: { urlSlug: slug },
    });

    if (postSlug) {
      const error = new Error('Post with same url already exist');
      error.statusCode = 404;
      throw error;
    }

    const data = new Post({
      title,
      body,
      author,
      urlSlug: slug,
    });

    await data.save();

    res.status(201).json({
      message: 'Post added successfully!',
      data,
    });
  } catch (err) {
    if (!err.statusCode) {
      err.statusCode = 500;
    }
    next(err);
  }
};

exports.editPost = async(req, res, next) => {
  // const result = validationResult(req).formatWith(errorFormatter);
  const id = req.params.id;
  const title = req.body.title;
  const body = req.body.body;
  const author = req.body.author;

  try {
    const post = await Post.findByPk(id);

    if (!post) {
      const error = new Error('post does not exist');
      error.statusCode = 404;
      throw error;
    }

    const data = await post.update({
      title,
      body,
      author,
    });

    res.status(201).json({
      message: 'Post updated!',
      data,
    });
  } catch (err) {
    if (!err.statusCode) {
      err.statusCode = 500;
    }
    next(err);
  }
};

exports.deletePost = async(req, res, next) => {
  // const result = validationResult(req).formatWith(errorFormatter);

  const id = req.params.id;
  console.log('deleting post id: ' + id);

  try {
    const destroyed = await Post.destroy({
      where: {
        id,
      },
    });

    if (!destroyed) {
      const error = new Error('Post not found. post not deleted');
      error.statusCode = 404;
      throw error;
    }

    res.status(201).json({
      message: 'Post added successfully!',
      destroyed,
    });
  } catch (err) {
    if (!err.statusCode) {
      err.statusCode = 500;
    }
    next(err);
  }
};
